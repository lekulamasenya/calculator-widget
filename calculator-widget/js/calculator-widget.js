function GetTodaysDate() {
    var today = new Date().toISOString().slice(0, 10);
    document.getElementById("dateOfInvestment").innerHTML = "<b>Date of investment: </b>" + today;
}

function GetIntraDaySharePrice() {
    if (IsValidAmountToInvest()) {
        // var url = "https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol=CIP.JO&apikey=KQ588EDRT9JQOC82";
        var url = "https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol=CPI.JO&apikey=KQ588EDRT9JQOC82";

        fetch(url)
        .then(function(response) {
            return response.json();
        }).then(function(jsonData) {
            var lastestData = Object.keys(jsonData["Time Series (Daily)"])[0];
            var data = jsonData["Time Series (Daily)"][lastestData];
            var openPrice = data["1. open"];
            var highPrice = data["2. high"];
            var lowPrice = data["3. low"];
            var closePrice = data["4. close"];
            var volumePrice = data["5. volume"];

            var inputAmount = document.getElementById("amountToInvest").value;
            var sharesBought = inputAmount / (lowPrice / 100);
            var sharesBoughtRounded = Math.round( sharesBought * 10 ) / 10;
            document.getElementById("sharedBought").innerHTML = sharesBoughtRounded;
            document.getElementById("shareholdingValue").innerHTML = inputAmount;
            document.getElementById("SharePrice").innerHTML = parseFloat(Math.round(lowPrice / 100) * 1).toFixed(2);
            document.getElementById("calculationsDiv").style.display = "block";
        }).catch(function(err) {
            console.log("Opps, Something went wrong!", err);
        })
   
        return false;
    } 
}

function IsValidAmountToInvest() {
    var inputValue;
    inputValue = document.getElementById("amountToInvest").value;
    if (isNaN(inputValue) || inputValue < 0) {
        return false;
    } else {
        return true;
    }
}